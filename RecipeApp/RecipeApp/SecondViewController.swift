//
//  SecondViewController.swift
//  RecipeApp
//
//  Created by Heather Wilcox on 3/1/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var object:Dictionary = [String:Any]()
    
    @IBOutlet var titleTxtVw: UITextView!
    @IBOutlet var ingredientTxtVw: UITextView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var descriptionTxtVw: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.titleTxtVw.text = (object["name"] as! String)
        self.descriptionTxtVw.text = object["wikiEntry"] as! String
        self.ingredientTxtVw.text = object["ingredients"] as! String
        
        let url = URL(string: object["image"] as! String)
        let data = NSData(contentsOf: url!)
        self.imageView.image = UIImage(data: data! as Data) //Error Here
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
